import unittest

from . import helper


class HeadTest(unittest.TestCase):

	def testHeadEmpty(self):
		data = helper.getData("""head{
			}""")
		self.assertEqual(len(data.head), 1)
		self.assertTrue('flags' in data.head)
		self.assertEqual(len(data.head['flags']), 0)

	def testHeadNormal(self):
		data = helper.getData("""head{
			9 lowestRecPPEM
			}""")
		self.assertEqual(len(data.head), 2)
		self.assertEqual(data.head['lowestRecPPEM'], 9)

	def testHeadFlags(self):
		data = helper.getData("""head{
			0 flags.instructionsMayDependOnPointSize
			1 flags.forcePpemToIntegerValues
			0 flags.instructionsMayAlterAdvanceWidth
			1 flags.fontOptimizedForClearType
			}""")
		flags = data.head['flags']
		self.assertEqual(len(flags), 4)
		self.assertEqual(flags[0], ( 2,0) )
		self.assertEqual(flags[1], ( 3,1) )
		self.assertEqual(flags[2], ( 4,0) )
		self.assertEqual(flags[3], (13,1) )


class MaxpTest(unittest.TestCase):

	def testMaxpEmpty(self):
		data = helper.getData("""maxp{
			}""")
		self.assertEqual(len(data.maxp), 0)

	def testMaxpPart(self):
		data = helper.getData("""maxp{
			256 maxStackElements
			 64 maxFunctionDefs
			}""")
		self.assertEqual(len(data.maxp), 2)
		self.assertEqual(data.maxp['maxStackElements'], 256)
		self.assertEqual(data.maxp['maxFunctionDefs'], 64)

	def testMaxpFull(self):
		data = helper.getData("""maxp{
			256 maxStackElements
			 64 maxFunctionDefs
			 32 maxStorage
			  2 maxZones
			 16 maxTwilightPoints
			}""")
		self.assertEqual(len(data.maxp), 5)
		self.assertEqual(data.maxp['maxStackElements'], 256)
		self.assertEqual(data.maxp['maxFunctionDefs'], 64)
		self.assertEqual(data.maxp['maxStorage'], 32)
		self.assertEqual(data.maxp['maxZones'], 2)
		self.assertEqual(data.maxp['maxTwilightPoints'], 16)


class GaspTest(unittest.TestCase):

	def testGaspEmpty(self):
		data = helper.getData("""gasp{
			}""")
		self.assertEqual(len(data.gasp), 0)

	def testGasp(self):
		data = helper.getData("""gasp{
			    7
			   12           doGray symSmoothing
			  128 doGridfit                     symGridfit
			65535 doGridfit doGray symSmoothing symGridfit
			}""")
		self.assertEqual(data.gasp[0], (7, False, False, False, False))
		self.assertEqual(data.gasp[1], (12, False, True, True, False))
		self.assertEqual(data.gasp[2], (128, True, False, False, True))
		self.assertEqual(data.gasp[3], (65535, True, True, True, True))


class CvtTest(unittest.TestCase):

	def testCvtEmpty(self):
		data = helper.getData("""cvt{
			}""")
		self.assertEqual(len(data.cvt), 0)

	def testCvtBoundary(self):
		data = helper.getData("""cvt{
			 0 c0
			-0 c1
			 64 c2
			-64 c3
			 32767 c4
			-32768 c5
			-1 c6
			}""")
		self.assertEqual(len(data.cvt), 7)
		self.assertEqual(data.cvt, [0, 0, 64, -64, 32767, -32768, -1])


class FpgmPrepTest(unittest.TestCase):

	def testFpgmEmpty(self):
		code = helper.toBytes("", "", "fpgm")
		self.assertEqual(code, b"")

	def testPrepEmpty(self):
		code = helper.toBytes("", "", "prep")
		self.assertEqual(code, b"")
