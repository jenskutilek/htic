## 3.5.0 (2020-05-08)

- Added support for raw DELTA instructions.
- Fixed composite glyphs for FontTools.
- To accommodate unusual glyph names by FontTools,
  comments must be preceded by whitespace if they
  do not appear at the beginning of a line.
